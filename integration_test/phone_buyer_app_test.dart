import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/screen/home_page.dart';
import 'package:provider/provider.dart';

late MobileList mobileList;
Widget createHomepage() => ChangeNotifierProvider<MobileList>(
      create: (context) {
        mobileList = MobileList();
        return mobileList;
      },
      child: const MaterialApp(
        home: HomePage(),
      ),
    );
void addItems() {
  final mobile = Mobile(
    id: '123',
    name: 'Eg4',
    brand: 'Asus',
    rating: 4.8,
    thumbImageURL:
        'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
    price: 199,
    description: 'Goog phone',
  );
  mobileList.favoriteItemList.add(mobile);
}

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('mobile phone buyer integrationtest',
      (WidgetTester tester) async {
    await tester.pumpWidget(createHomepage());
    await Future.delayed(const Duration(seconds: 7), () {});

    await tester.tap(find.byKey(const Key('sortingbutton')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('lowToHigh')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('sortingbutton')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('highToLow')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('sortingbutton')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('ratingSort')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('favcard')).first);
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('favoriteListTab')).first);
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.tap(find.byKey(const Key('sizeboxcard')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 2));

    await tester.tap(find.byKey(const Key('backbutton')));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    await tester.drag(find.byType(Dismissible), const Offset(500.0, 0.0));
    await tester.pumpAndSettle();
    await tester.pump(const Duration(seconds: 1));

    expect(MobileList().favoriteItemList.length, 0);
  });
}
