import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/screen/mobile_detail_page.dart';
import 'package:mobile_phone_buyer/widget/mobile_information.dart';

void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });
  testWidgets('Test widgets in mobile detail page', (WidgetTester tester) async {
    Mobile mobile = Mobile(
      id: '123',
      name: 'Eg4',
      brand: 'Asus',
      rating: 4.8,
      thumbImageURL: 'https',
      price: 199,
      description: 'Goog phone',
    );

    await tester.pumpWidget(
        MobileDetailPage(
          mobile: mobile,
        ),);
    
    expect(find.byType(IconButton), findsWidgets);
    expect(find.byType(MobileInfomation), findsWidgets);
  });
}