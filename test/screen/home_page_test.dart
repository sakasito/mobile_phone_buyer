import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/screen/home_page.dart';
import 'package:provider/provider.dart';

late MobileList mobileList;

  final mobile = Mobile(
    id: '123',
    name: 'Eg4',
    brand: 'Asus',
    rating: 4.8,
    thumbImageURL: 'https',
    price: 199,
    description: 'Goog phone',
  );

Widget createsorting() => ChangeNotifierProvider<MobileList>(
      create: (context) {
        mobileList = MobileList();
        return mobileList;
      },
      child: const MaterialApp(
        home: HomePage(),
      ),
    );
void addItems() {

    mobileList.favoriteItemList.add(mobile);
}

void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });
  testWidgets('Test tabbar in homePage', (WidgetTester tester) async {
    await tester.pumpWidget(
      ChangeNotifierProvider(
        create: (context) => MobileList(),
        child: const HomePage(),
      ),
    );
    Finder mobileTab = find.byKey(const Key('mobileTab'));
    Finder sortingbutton = find.byKey(const Key('sortingbutton'));
    Finder mobileListTab = find.byKey(const Key('mobileListTab'));
    Finder favoriteListTAB = find.byKey(const Key('favoriteListTab'));
    expect(find.byType(TabBarView), findsOneWidget);
    expect(mobileTab.toString().contains('mobileTab'), true);
    expect(sortingbutton, findsOneWidget);
    expect(find.byKey(const Key('mobileListTab')), findsOneWidget);
    expect(find.byKey(const Key('favoriteListTab')), findsOneWidget);
    expect(favoriteListTAB, findsOneWidget);
    expect(mobileListTab, findsOneWidget);
  });

  testWidgets('Test if ListView shows up', (tester) async {
    await tester.pumpWidget(createsorting());
    addItems();
    await tester.tap(find.byIcon(Icons.sort));
    await tester.pumpAndSettle();
    expect(find.byKey(const Key('lowToHigh')), findsOneWidget);
    expect(find.byType(TextButton), findsWidgets);
  });
}
