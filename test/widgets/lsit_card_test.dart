import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/widget/list_card.dart';
import 'package:mobile_phone_buyer/widget/list_card_detail.dart';
import 'package:provider/provider.dart';

late MobileList mobileList;

late Mobile mobile = Mobile(
    id: '123',
    name: 'Eg4',
    brand: 'Asus',
    rating: 4.8,
    thumbImageURL: 'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
    price: 199,
    description: 'Goog phone',
  );
Widget createListCard() => ChangeNotifierProvider<MobileList>(
  
      create: (context) {
        mobileList = MobileList();
        return mobileList;
      },
      child:  MaterialApp(
        home: ListCard(mobile: mobile,),
      ),
    );
void addItems() {
  final mobile = Mobile(
    id: '123',
    name: 'Eg4',
    brand: 'Asus',
    rating: 4.8,
    thumbImageURL: 'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
    price: 199,
    description: 'Goog phone',
  );
    mobileList.favoriteItemList.add(mobile);
}

void main() {
    setUpAll(() {
    HttpOverrides.global = null;
  });
  testWidgets('Test widgets in ListCard', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: createListCard()
      ),
    ));
    Finder finder = find.byWidgetPredicate((w) => w is ListTile);
    expect(finder, findsWidgets);
    expect(find.byType(Card), findsOneWidget);
    expect(find.byType(ClipRRect), findsOneWidget);
    expect(find.byType(ListCardDetail), findsOneWidget);
    expect(find.byType(IconButton), findsOneWidget);
  });

  testWidgets('Test texts in ListCardDetail', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: createListCard()
      ),
    ));

    expect(find.text('Rating: 4.8'), findsWidgets);
    expect(find.text('Price: 199.0\$.'), findsWidgets);
    expect(find.text('Goog phone'), findsWidgets);
  });
}
