import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/widget/list_card.dart';
import 'package:mobile_phone_buyer/widget/mobile_list_view.dart';
import 'package:provider/provider.dart';

List<Mobile> mobiles = [];
late MobileList mobileList;
TabController? tabController;

Mobile mobile = Mobile(
  id: '123',
  name: 'Eg4',
  brand: 'Asus',
  rating: 4.8,
  thumbImageURL: 'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
  price: 199,
  description: 'Goog phone',
);

List<Tab> myTabs = <Tab>[
  const Tab(text: 'Mobile List'),
  const Tab(text: 'Favorite List'),
];

void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });
  testWidgets('Test Widget in MobileInfomation', (WidgetTester tester) async {
    Finder finder = find.byWidgetPredicate((w) => w is ListCard);
    tabController =
        TabController(vsync: const TestVSync(), length: myTabs.length);
    mobiles.add(mobile);
    await tester.pumpWidget(ChangeNotifierProvider<MobileList>(
      create: (context) {
        mobileList = MobileList();
        return mobileList;
      },
      child: MaterialApp(
        home: MobileListView(
          mobiles: mobiles,
          tabController: tabController,
        ),
      ),
    ));

    expect(find.byType(TabBarView), findsWidgets);
    expect(find.byType(ListView), findsWidgets);
    expect(finder, findsWidgets);
  });
}
