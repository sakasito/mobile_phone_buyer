import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_detail.dart';
import 'package:mobile_phone_buyer/widget/mobile_information.dart';

 List<MobileDetail> mobiles = [];
    Mobile mobile = Mobile(
      id: '123',
      name: 'Eg4',
      brand: 'Asus',
      rating: 4.8,
      thumbImageURL: 'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
      price: 199,
      description: 'Goog phone',
    );

    MobileDetail mobileDetail = MobileDetail(
      id: '123',
      mobileId: '1',
      url: 'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
    );

void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });
  testWidgets('Test texts in MobileInfomation', (WidgetTester tester) async {

    mobiles.add(mobileDetail);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MobileInfomation(
          mobile: mobile,
          mobiles: mobiles,
        ),
      ),
    ));
    expect(find.text('Rating: 4.8'), findsWidgets);
    expect(find.text('Price: 199.0\$.'), findsWidgets);
    expect(find.text('Goog phone'), findsWidgets);
  });

    testWidgets('Test widgets in MobileInfomation', (WidgetTester tester) async {

    mobiles.add(mobileDetail);
    await tester.pumpWidget(MaterialApp(
      home: Scaffold(
        body: MobileInfomation(
          mobile: mobile,
          mobiles: mobiles,
        ),
      ),
    ));
    expect(find.byType(ClipRRect), findsWidgets);
  });
}
