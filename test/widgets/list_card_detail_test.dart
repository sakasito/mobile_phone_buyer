
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/widget/list_card_detail.dart';

Mobile mobile = Mobile(
      id: '123',
      name: 'Eg4',
      brand: 'Asus',
      rating: 4.8,
      thumbImageURL: 'https://scb-test-mobile.herokuapp.com/api/mobiles/1/images/',
      price: 199,
      description: 'Goog phone',
    );
void main() {

  testWidgets('Test Widgets in ListCardDetail', (WidgetTester tester) async {

    await tester.pumpWidget( MaterialApp(
    home: Scaffold(
      body: ListCardDetail(mobile: mobile,),
    ),
  ));

    expect(find.byType(Text), findsWidgets); 
    expect(find.byType(SizedBox), findsWidgets); 
  });

  testWidgets('Test list detail in ListCardDetail', (WidgetTester tester) async {

    await tester.pumpWidget( MaterialApp(
    home: Scaffold(
      body: ListCardDetail(mobile: mobile,),
    ),
  ));

    expect(find.text('Rating: 4.8'), findsWidgets); 
    expect(find.text('Price: 199.0\$.'), findsWidgets); 
    expect(find.text('Goog phone'), findsWidgets); 
  });


}
