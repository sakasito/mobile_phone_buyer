import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';

final mobileList = MobileList();
Mobile mobile = Mobile(
  name: 'Eg4',
  id: '1',
  brand: 'sansung',
  rating: 4.5,
  thumbImageURL: 'html',
  price: 499,
  description: 'des',
);
void main() {
  tearDown(() {
    mobileList.favoriteItemList.clear();
  });
  testWidgets('Test when adding the favorite using toggle',
      (WidgetTester tester) async {
    mobileList.toggleFavorit(mobile);

    expect(mobileList.favoriteItemList[0].name, 'Eg4');
  });

  testWidgets('Test when removing the favorite using toggle',
      (WidgetTester tester) async {
    mobileList.toggleFavorit(mobile);
    mobileList.toggleFavorit(mobile);
    expect(mobileList.favoriteItemList.length, 0);
  });

  testWidgets('Test when removing the favorite', (WidgetTester tester) async {
    mobileList.toggleFavorit(mobile);
    mobileList.removeFavorit(0);
    expect(mobileList.favoriteItemList.length, 0);
  });
}
