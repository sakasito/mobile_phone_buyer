import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';

void main() {
  testWidgets('Test mobile model', (WidgetTester tester) async {

    Mobile mobile = Mobile(
      name: 'Eg4',
      id: '1',
      brand: 'sansung',
      rating: 4.5,
      thumbImageURL: 'html',
      price: 499,
      description: 'des',
    );

    expect(mobile.name, 'Eg4');
    expect(mobile.id, '1');
    expect(mobile.brand, 'sansung');
    expect(mobile.rating, 4.5);
    expect(mobile.thumbImageURL, 'html');
    expect(mobile.price, 499);
    expect(mobile.description, 'des');
  });
}
