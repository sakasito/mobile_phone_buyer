import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';

void main() {
  testWidgets('Test mobile model list', (WidgetTester tester) async {
    final mobileList = MobileList();

    final mobile1 = Mobile(
      id: '123',
      name: 'Eg4',
      brand: 'Asus',
      rating: 4.8,
      thumbImageURL: 'https',
      price: 199,
      description: 'Goog phone',
    );

    mobileList.toggleFavorit(mobile1);
    expect(mobileList.favoriteItemList.length, 1);

    mobileList.removeFavorit(0);
    expect(mobileList.favoriteItemList.length, 0);
  });
}
