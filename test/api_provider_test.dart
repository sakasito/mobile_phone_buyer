import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;

void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });
  testWidgets(
    'Test with HTTP enabled',
    (tester) async {
      await tester.runAsync(() async {
        // Use `runAsync` to make real asynchronous calls
        expect(
          // https://mocki.io/v1/2ffee543-5f59-4068-ba4b-f334e08baa61
          // (await http.Client().get(Uri.parse('https://cors-anywhere.herokuapp.com/https://scb-test-mobile.herokuapp.com/api/mobiles'))).statusCode,
          (await http.get(Uri.parse(
                  'https://mocki.io/v1/2ffee543-5f59-4068-ba4b-f334e08baa61')))
              .statusCode,
          200,
        );
      });
    },
  );
}
