import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/widget/mobile_list_view.dart';
import 'package:provider/provider.dart';
import '../service/api_provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late List data;
  bool isLoading = false;
  List<Mobile> favoriteList = <Mobile>[];
  List<Mobile> mobiles = [];
  bool sortPrice = true;
  late TabController _tabController;

  @override
  void initState() {
    getData();
    _tabController = TabController(vsync: this, length: myTabs.length);
    super.initState();
  }

    static const List<Tab> myTabs = <Tab>[
    Tab(text: 'Mobile List', key: Key('mobileListTab'),),
    Tab(text: 'Favorite List', key: Key('favoriteListTab')),
  ];





 @override
 void dispose() {
   _tabController.dispose();
   super.dispose();
 }

  Future<String> getData() async {
    try {
      Future<dynamic> future = ApiProvider()
          .get('https://scb-test-mobile.herokuapp.com/api/mobiles', true);
      Mobile mobile;
      future.then((value) => {
            for (var mob in value)
              {
                mobile = Mobile(
                  name: mob['name'],
                  id: mob['id'].toString(),
                  brand: mob['brand'],
                  rating: mob['rating'],
                  thumbImageURL: mob['thumbImageURL'],
                  price: mob['price'],
                  description: mob['description'],
                ),
                mobiles.add(mobile),
              },
            setState(() {
              isLoading = false;
            }),
          });
    } catch (e) {
      throw Exception('[Catch]Error fetching mobile list: $e');
    }
    return 'Success';
  }

  void sortByRating(MobileList favoriteList) {
    setState(() {
      mobiles.sort((b, a) => a.rating!.compareTo(b.rating!));
      favoriteList.favoriteItemList
          .sort((b, a) => a.rating!.compareTo(b.rating!));
    });
    Navigator.pop(context, 'Cancel');
  }

  void sortByPrice(
    String htl,
    MobileList favoriteList,
  ) {
    if (htl == 'HTL') {
      setState(() {
        sortPrice = false;
        favoriteList.favoriteItemList
            .sort((b, a) => a.price!.compareTo(b.price!));
        mobiles.sort((b, a) => a.price!.compareTo(b.price!));
      });
    } else {
      setState(() {
        sortPrice = true;
        favoriteList.favoriteItemList
            .sort((a, b) => a.price!.compareTo(b.price!));
        mobiles.sort((a, b) => a.price!.compareTo(b.price!));
      });
    }
    Navigator.pop(context, 'Cancel');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.lightBlue[50],
        appBar: AppBar(
          bottom:  TabBar(
            key: const Key('tabBarView'),
            controller: _tabController,
            tabs: myTabs,
          ),
          actions: <Widget>[
            IconButton(
               key: const Key('sortingbutton'),
              icon: const Icon(Icons.sort),
              onPressed: () {
                showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    content: Consumer<MobileList>(
                      builder: (context, favoriteList, child) {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            TextButton(
                              key: const Key('lowToHigh'),
                              onPressed: () => sortByPrice('HTL', favoriteList),
                              child: const Text('Higher price to lower price'),
                            ),
                            const Padding(padding: EdgeInsets.all(5)),
                            TextButton(
                              key: const Key('highToLow'),
                              onPressed: () => sortByPrice('', favoriteList),
                              child: const Text('Lower price to higher price'),
                            ),
                            const Padding(padding: EdgeInsets.all(5)),
                            TextButton(
                              key: const Key('ratingSort'),
                              onPressed: () => sortByRating(favoriteList),
                              child: const Text('Sort by rating'),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                );
              },
            )
          ],
        ),
        body: MobileListView(mobiles: mobiles, tabController :_tabController),
      ),
    ));
  }
}
