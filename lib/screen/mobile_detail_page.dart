import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile_detail.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/widget/mobile_information.dart';

import '../service/api_provider.dart';

class MobileDetailPage extends StatefulWidget {
  final Mobile mobile;
  const MobileDetailPage({Key? key, required this.mobile}) : super(key: key);

  @override
  _MobileDetailPageState createState() => _MobileDetailPageState();
}

class _MobileDetailPageState extends State<MobileDetailPage> {
  late List data;
  bool isLoading = true;
  List<MobileDetail> mobiles = [];

  @override
  void initState() {
    getData(widget.mobile.id!);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<String> getData(String id) async {
    try {
      Future<dynamic> future = ApiProvider().get(
          'https://scb-test-mobile.herokuapp.com/api/mobiles/$id/images/',
          false);
      MobileDetail mobile;
      future.then((value) => {
            for (var mob in value)
              {
                mobile = MobileDetail(
                  id: mob['id'].toString(),
                  mobileId: mob['mobile_id'].toString(),
                  url: mob['url'].toString(),
                ),
                mobiles.add(mobile),
              },
            setState(() {
              isLoading = false;
            }),
          });
    } catch (e) {
      throw Exception('[Catch]Error fetching mobile detail: $e');
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    String? title = widget.mobile.name;
    Mobile mobile = widget.mobile;
    return MaterialApp(
        title: title!,
        home: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton(
              key: const Key('backbutton'),
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(title),
          ),
          body: Column(
            children: <Widget>[
              SingleChildScrollView(
                  child: MobileInfomation(
                mobile: mobile,
                mobiles: mobiles,
              ))
            ],
          ),
        ));
  }
}
