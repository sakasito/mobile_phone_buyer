import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';

class ListCardDetail extends StatelessWidget {
  final Mobile? mobile;

  const ListCardDetail({Key? key, required this.mobile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width / 2,
          height: 45,
          child: Text(
            mobile!.description!,
            maxLines: 2,
            overflow: TextOverflow.fade,
            softWrap: true,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
              height: 60,
              child: SizedBox(
                child: Text(
                  'Price: ${mobile!.price!}\$.',
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
              height: 60,
              child: SizedBox(
                child: Text(
                  'Rating: ${mobile!.rating!}',
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
