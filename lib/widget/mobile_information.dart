import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_detail.dart';

class MobileInfomation extends StatelessWidget {
  final Mobile? mobile;
  final List<MobileDetail> mobiles;

  const MobileInfomation(
      {Key? key, required this.mobile, required this.mobiles})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Stack(
          children: [
            SizedBox(
              key:  const Key('mobileDetailImage'),
                height: 200.0,
                child: ListView.builder(
                    padding: const EdgeInsets.all(15),
                    scrollDirection: Axis.horizontal,
                    itemCount: mobiles.length,
                    itemBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.35,
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10.0)),
                          child: Image.network(mobiles[index].url!),
                        ),
                      );
                    })),
                    Container(
              height: 50.0,
              color: Colors.grey[400]!.withOpacity(0.3),
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              alignment: Alignment.centerLeft,
              child: SizedBox(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Rating: ${mobile!.rating}'),
                  Text('Price: ${mobile!.price}\$.'),
                ],
              )),
            ),
          ],
        ),
        SingleChildScrollView(
            child: Column(
          children: <Widget>[
            Container(
                margin: const EdgeInsets.symmetric(vertical: 20.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(10),
                        child: Text('${mobile!.description}'),
                      )
                    ],
                  ),
                ))
          ],
        ))
      ],
    );
  }
}
