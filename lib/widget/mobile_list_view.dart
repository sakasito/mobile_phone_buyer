import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/widget/list_card.dart';
import 'package:provider/provider.dart';

class MobileListView extends StatefulWidget {
  final List<Mobile> mobiles;
  final TabController? tabController;

  const MobileListView(
      {Key? key, required this.mobiles, this.tabController})
      : super(key: key);

  @override
  _MobileListViewState createState() => _MobileListViewState();
}

class _MobileListViewState extends State<MobileListView>
    with SingleTickerProviderStateMixin {


  @override
  void initState() {
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return TabBarView(
      controller: widget.tabController,
      children: [
        Container(
          key: const Key('mobileListView'),
          child: Consumer<MobileList>(
            builder: (context, favoriteList, child) {
              return ListView.builder(
                  key: const Key('listView'),
                  padding: const EdgeInsets.all(15),
                  itemCount: widget.mobiles.length,
                  itemExtent: 120,
                  itemBuilder: (BuildContext context, int index) {
                    return ListCard(
                      key: Key(widget.mobiles[index].id!),
                      mobile: widget.mobiles[index],
                    );
                  });
            },
          ),
        ),
        Consumer<MobileList>(
          builder: (context, favoriteList, child) {
            return ListView.builder(
                padding: const EdgeInsets.all(15),
                itemCount: favoriteList.favoriteItemList.length,
                itemExtent: 120,
                itemBuilder: (BuildContext context, int index) {
                  return Dismissible(
                      key: UniqueKey(),
                      onDismissed: (direction) {
                        setState(() {
                          favoriteList.removeFavorit(index);
                        });
                      },
                      child: ListCard(
                          mobile: favoriteList.favoriteItemList[index],
                          isInFavorite: true));
                });
          },
        )
      ],
    );
  }
}
