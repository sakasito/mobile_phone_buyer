import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/model/mobile.dart';
import 'package:mobile_phone_buyer/model/mobile_list.dart';
import 'package:mobile_phone_buyer/screen/mobile_detail_page.dart';
import 'package:provider/provider.dart';

import 'list_card_detail.dart';

class ListCard extends StatefulWidget {
  const ListCard({
    Key? key,
    required this.mobile,
    final this.isInFavorite = false,
  }) : super(key: key);

  final Mobile mobile;
  final bool? isInFavorite;
  @override
  _ListCardState createState() => _ListCardState();
}

class _ListCardState extends State<ListCard> {

  bool isFavorite(List<Mobile> mobiles, Mobile mobile) {
    bool isContains = mobiles.contains(mobile);
    return isContains;
  }

  void redirectToDetail(BuildContext context, Mobile mobile) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MobileDetailPage(mobile: mobile)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        contentPadding: const EdgeInsets.only(top: 10),
        leading: SizedBox(
          key: const Key('sizeboxcard'),
          width: MediaQuery.of(context).size.width  * 0.2,
          height: 100,
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10.0)),
            child: Image.network(widget.mobile.thumbImageURL!),
          ),
        ),
        title: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Text(widget.mobile.name!),
        ),
        subtitle:ListCardDetail(mobile: widget.mobile),
        trailing: widget.isInFavorite == true
            ? null
            : Consumer<MobileList>(
                builder: (context, favoriteList, child) {
                  return IconButton(
                    key:  const Key('favcard'),
                    icon: Icon(
                      isFavorite(favoriteList.favoriteItemList, widget.mobile)
                          ? Icons.star
                          : Icons.star_border,
                      color: isFavorite(
                              favoriteList.favoriteItemList, widget.mobile)
                          ? Colors.pink
                          : null,
                    ),
                    onPressed: () {
                      favoriteList.toggleFavorit(widget.mobile);
                      setState(() {});
                    },
                  );
                },
              ),
        onTap: () => redirectToDetail(context, widget.mobile),
      ),
    );
  }
}
