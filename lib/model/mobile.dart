import 'package:flutter/material.dart';

class Mobile {
  static BuildContext? context;
  final String? name;
  final String? id;
  final String? brand;
  final double? rating;
  final String? thumbImageURL;
  final double? price;
  final String? description;

  Mobile({
    this.id,
    this.name,
    this.brand,
    this.rating,
    this.thumbImageURL,
    this.price,
    this.description,
  });

  factory Mobile.fromJson(Map<String, dynamic> json) {
    return Mobile(
      id: json['id'],
      name: json['name'],
      brand: json['brand'],
      rating: json['rating'],
      thumbImageURL: json['thumbImageURL'],
      price: json['price'],
      description: json['description'],
    );
  }

}
