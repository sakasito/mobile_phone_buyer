
import 'package:flutter/material.dart';

class MobileDetail {
  static BuildContext? context;
  final String? id;
  final String? mobileId;
  final String? url;
  
  MobileDetail({
    this.id,
    this.mobileId,
    this.url,
  });

  factory MobileDetail.fromJson(Map<String, dynamic> json) {
    return MobileDetail(
      id: json['id'],
      mobileId: json['mobile_id'],
      url: json['url'],
    );
  }
}