import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile_phone_buyer/screen/home_page.dart';
import 'package:provider/provider.dart';

import 'model/mobile_list.dart';

void main() {
  Provider.debugCheckInvalidValueType = null;
  runZonedGuarded(() {
    runApp(
      ChangeNotifierProvider(
        create: (context) => MobileList(),
        child: const MyApp(),
      ),
    );
  }, (Object error, StackTrace stack) {
    throw Exception('Error in main $error, $stack');
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: HomePage());
  }
}
